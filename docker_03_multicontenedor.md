# 3. Multicontenedor
## 3.1. Crear red
`docker network create xarxa_dades`
~~~
a5bc10eb1e8b33a46e2a478aa2015981f11cda6f7555473a23a66c707ff780b0
~~~

`docker network inspect xarxa_dades`

~~~
[
    {
        "Name": "xarxa_dades",
        "Id": "a5bc10eb1e8b33a46e2a478aa2015981f11cda6f7555473a23a66c707ff780b0",
        "Created": "2019-10-25T17:12:44.585727866+02:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.19.0.0/16",
                    "Gateway": "172.19.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {},
        "Options": {},
        "Labels": {}
    }
]
~~~

## 3.2. MariaDB
`docker run --network xarxa_dades --name mi_db -e MYSQL_ROOT_PASSWORD=p@ssw0rd -d mariadb`

~~~
Unable to find image 'mariadb:latest' locally
latest: Pulling from library/mariadb
22e816666fd6: Pull complete
079b6d2a1e53: Pull complete
11048ebae908: Pull complete
c58094023a2e: Pull complete
1e8f13102fa0: Pull complete
8c1425d731a6: Pull complete
14e6f69e6aab: Pull complete
c90c2f3858cf: Pull complete
b78202ba9229: Pull complete
cadce28d1b9c: Pull complete
6fb2c5af5492: Pull complete
7a59522b36b8: Pull complete
722b05c4c4b1: Pull complete
bd4039b5406f: Pull complete
Digest: sha256:7b371982ac83beee40bee645c6efab1bc9113abbce9cbc95bf3eddac268adf57
Status: Downloaded newer image for mariadb:latest
be1ba3ee3e26bf14ab45f18bfb4c4ff2ceb8c24d16ada50f8cd7c5f71e1417c0
~~~

## 3.3. Mediawiki
`docker run --network xarxa_dades --name mi_wiki -e MYSQL_DATABASE=wiki -e MYSQL_USER=root -e MYSQL_PASSWORD=p@ssw0rd -e MYSQL_RANDOM_ROOT_PASSWORD='no' -p 8080:80 -d mediawiki`

`docker ps -a`

~~~

CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
32f712693175        wordpress           "docker-entrypoint.s…"   13 days ago         Exited (0) 6 minutes ago                        angry_wu
1ccd871b43d0        mediawiki           "docker-php-entrypoi…"   13 days ago         Exited (0) 54 seconds ago                       mi_wiki
aff2512e04f0        wordpress           "docker-entrypoint.s…"   2 weeks ago         Exited (0) 5 minutes ago                        mi_wordpress
be1ba3ee3e26        mariadb             "docker-entrypoint.s…"   2 weeks ago         Exited (0) 5 seconds ago                        mi_db
~~~
_**Aqui tenim els contenidors creats però no engegats**_

`docker start mi_wiki`

`docker start mi_db`

`docker ps -a`

~~~

CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                     PORTS                  NAMES
32f712693175        wordpress           "docker-entrypoint.s…"   13 days ago         Exited (0) 9 minutes ago                          angry_wu
1ccd871b43d0        mediawiki           "docker-php-entrypoi…"   13 days ago         Up 17 seconds              0.0.0.0:8080->80/tcp   mi_wiki
aff2512e04f0        wordpress           "docker-entrypoint.s…"   2 weeks ago         Exited (0) 8 minutes ago                          mi_wordpress
be1ba3ee3e26        mariadb             "docker-entrypoint.s…"   2 weeks ago         Up 11 seconds              3306/tcp               mi_db
~~~
_**Ara si que els tenim en marxa els contenidors.**_

_**Ja podem començar a configurar la nostra MediaWiki**_

![MediaWiki](mediawiki.png)

![MediaWiki](mediawiki_1.png)

![MediaWiki](mediawiki_2.png)

![MediaWiki](mediawiki_3.png)

![MediaWiki](mediawiki_4.png)

![MediaWiki](mediawiki_5.png)

![MediaWiki](mediawiki_6.png)

![MediaWiki](mediawiki_7.png)

![MediaWiki](mediawiki_8.png)

![MediaWiki](mediawiki_9.png)
