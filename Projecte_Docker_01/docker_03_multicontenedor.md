# PRE-PROJECTE

1. Inicia un contenidor amb Ubuntu  com a imatge base, exportant el port 80

`docker run -it --name apache2 -p8080:80 ubuntu`

~~~
Unable to find image 'ubuntu:latest' locally
latest: Pulling from library/ubuntu
7ddbc47eeb70: Pull complete 
c1bbdc448b72: Pull complete 
8c3b70e39044: Pull complete 
45d437916d57: Pull complete 
Digest: sha256:6e9f67fa63b0323e9a1e587fd71c561ba48a034504fb804fd26fd8800039835d
Status: Downloaded newer image for ubuntu:latest
383d944c03958888b0893a4628466e712be851815e3fe1ba5d87d50dee4be9ad
~~~

`docker ps -a`

~~~
CONTAINER ID        IMAGE                COMMAND                  CREATED             STATUS                  PORTS                  NAMES
3cd8395f1414        ubuntu               "/bin/bash"              16 minutes ago      Up 7 minutes            0.0.0.0:82->80/tcp     apache2
~~~

2. Engega el servidor web, però en mode foreground

`docker exec -i -t apache2 /bin/bash`

~~~
root@3cd8395f1414:/# /etc/init.d/apache2 status
 * apache2 is not running
root@3cd8395f1414:/# /etc/init.d/apache2 start 
 * Starting Apache httpd web server apache2
~~~

3. Esbrina la IP del contenidor i en el navegador web propi visualitza la pàgina inicial.

`docker container inspect apache2 | grep IPAddress`

~~~
            "SecondaryIPAddresses": null,
            "IPAddress": "172.17.0.2",
                    "IPAddress": "172.17.0.2",
~~~

4. Apunta a localhost:port per visualitzar la pàgina web.
5. Des d'un navegador d'un company intenta obrir la pàgina del teu propi servidor. Utilitza la IP del teu PC i el port que has exportat. Funciona amb la IP pròpia del contenidor?

![Apache2](apache2_1.png)

Utilitza la IP de la màquina virtual (HA D'ESTAR EN ADAPTADOR PONT)
NOTA: Jo utilitzo una amfitrió i hem connecto amb el Fedora perque no tenia a ningú

6. Fes commit del contenidor en una imatge anomenada "ProvaPau" (Pau serà el vostre nom).

`sudo docker commit apache2 provaalex`

~~~
sha256:6b05e992c5eb1dfb888c3e85505d8a292e55a8aa3674d1bd1c29db8643b44732
~~~

7. Canvia el tag de la imatge per posar davant el teu repositori de DockerHub. Després puja la imatge a DockerHub

`docker login -u asmon10`

~~~
Password: 
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
~~~

`docker commit -a "asmon10 <alex_sernamontore@iescarlesvallbona.cat >" apache2 asmon10/m11:apache2`

~~~
sha256:4e8a723a86869f04d59bffa4ab88c85e90cd88debadca2f55485f5548bea6568
~~~

`docker push asmon10/m11:apache2`

~~~
The push refers to repository [docker.io/asmon10/m11]
191afdc02c0e: Pushed 
e0b3afb09dc3: Mounted from library/ubuntu 
6c01b5a53aac: Mounted from library/ubuntu 
2c6ac8e5063e: Mounted from library/ubuntu 
cc967c529ced: Mounted from library/ubuntu 
apache2: digest: sha256:eb335aad6517476fa8457d5bcbef74a5395dc0ed5050670cacfc4007ae6d107d size: 1364
~~~

8. Comprova que hi és al DockerHub (via navegador web).

![DockerHUB](dockerhub_1.png)

9. Crea el Dockerfile de l'exercici anterior per a fer la provisió del contenidor (comandes executades).

~~~~
FROM ubuntu:16.04
RUN apt update -y && apt install apache2 -y && /etc/init.d/apache2 restart
EXPOSE 80
~~~~

10. Executa docker build per a generar una imatge anomenada "ApachePau" (Pau serà el vostre nom).

`docker build -t apachealex .`

~~~
   ...done.
Removing intermediate container 0691e49b4868
 ---> 410c587a0aca
Step 3/3 : EXPOSE 80
 ---> Running in 4de85233fdad
Removing intermediate container 4de85233fdad
 ---> e54d9779c252
Successfully built e54d9779c252
Successfully tagged apachealex:latest
~~~

11. Puja la imatge a DockerHub

`docker run -dt apachealex`

~~~
631add1fd5ebdb44f940e59a2d756ab561b0ccaa95d94d561e9c83e35ecd8d41
~~~

`docker ps -a`

~~~
CONTAINER ID        IMAGE                COMMAND                  CREATED             STATUS                  PORTS                  NAMES
631add1fd5eb        apachealex           "/bin/bash"              3 seconds ago       Up 2 seconds            80/tcp                 magical_snyder
3cd8395f1414        ubuntu               "/bin/bash"              About an hour ago   Up About an hour        0.0.0.0:82->80/tcp     apache2
0686c1df02ac        mediawiki            "docker-php-entrypoi…"   2 hours ago         Up 2 hours              0.0.0.0:8080->80/tcp   mi_wiki
672eea798cb4        joomla               "/entrypoint.sh apac…"   2 hours ago         Up 2 hours              0.0.0.0:81->80/tcp     mi_jm
ebf558d68a13        asmon10/m11:mi_db1   "docker-entrypoint.s…"   2 hours ago         Up 2 hours              3306/tcp               mi_db
32f712693175        wordpress            "docker-entrypoint.s…"   2 weeks ago         Exited (0) 6 days ago                          angry_wu
aff2512e04f0        wordpress            "docker-entrypoint.s…"   2 weeks ago         Exited (0) 6 days ago                          mi_wordpress
~~~

`docker commit -a "asmon10 <alex_sernamontore@iescarlesvallbona.cat>" magical_snyder asmon10/m11:apachealex1`

~~~
sha256:de2b39fcd0fbb8c0b4def548e9b5c94740dad4659099f34d26d4c7cedc59b3c9
~~~

`docker push asmon10/m11:apachealex1`

~~~
The push refers to repository [docker.io/asmon10/m11]
e4a5b0495e94: Pushed 
9acfe225486b: Pushed 
90109bbe5b76: Mounted from library/ubuntu 
cb81b9d8a6c9: Mounted from library/ubuntu 
ea69392465ad: Mounted from library/ubuntu 
apachealex1: digest: sha256:b17da362c76c956adddc177c9c5b4de542b984f5ebeb3363c75f174ea1144110 size: 1362
~~~

12. Crea un nou contenidor que executi la imatge que has pujat a DockerHub (posa el repositori davant el nom de la imatge). Fes-ho amb el paràmetre -dt (detach, terminal), per que no capturi la consola.
13. Crea un nou contenidor partint de la imatge ApachePau del repositori.
14. Exporta un port diferent pel servidor.
`docker run -dt -p83:80 asmon10/m11:apachealex1`

~~~
2e7c3270bd60f8472b9a2f6fc4fa3244270193bdf5be38ef4fda812bb5ac82fb
~~~

`docker ps -a`

~~~
CONTAINER ID        IMAGE                     COMMAND                  CREATED             STATUS                  PORTS                  NAMES
2e7c3270bd60        asmon10/m11:apachealex1   "/bin/bash"              54 seconds ago      Up 53 seconds           0.0.0.0:83->80/tcp     nostalgic_faraday
631add1fd5eb        apachealex                "/bin/bash"              9 minutes ago       Up 9 minutes            80/tcp                 magical_snyder
3cd8395f1414        ubuntu                    "/bin/bash"              2 hours ago         Up About an hour        0.0.0.0:82->80/tcp     apache2
0686c1df02ac        mediawiki                 "docker-php-entrypoi…"   2 hours ago         Up 2 hours              0.0.0.0:8080->80/tcp   mi_wiki
672eea798cb4        joomla                    "/entrypoint.sh apac…"   2 hours ago         Up 2 hours              0.0.0.0:81->80/tcp     mi_jm
ebf558d68a13        asmon10/m11:mi_db1        "docker-entrypoint.s…"   2 hours ago         Up 2 hours              3306/tcp               mi_db
32f712693175        wordpress                 "docker-entrypoint.s…"   2 weeks ago         Exited (0) 6 days ago                          angry_wu
aff2512e04f0        wordpress                 "docker-entrypoint.s…"   2 weeks ago         Exited (0) 6 days ago                          mi_wordpress
~~~

# 3. Multicontenedor
## 3.1. Crear red
`docker network create xarxa_dades`

~~~
a5bc10eb1e8b33a46e2a478aa2015981f11cda6f7555473a23a66c707ff780b0
~~~

`docker network inspect xarxa_dades`

~~~
[
    {
        "Name": "xarxa_dades",
        "Id": "a5bc10eb1e8b33a46e2a478aa2015981f11cda6f7555473a23a66c707ff780b0",
        "Created": "2019-10-25T17:12:44.585727866+02:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.19.0.0/16",
                    "Gateway": "172.19.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {},
        "Options": {},
        "Labels": {}
    }
]
~~~

## 3.2. MariaDB
`docker run --network xarxa_dades --name mi_db -e MYSQL_ROOT_PASSWORD=p@ssw0rd -d mariadb`

~~~
Unable to find image 'mariadb:latest' locally
latest: Pulling from library/mariadb
22e816666fd6: Pull complete
079b6d2a1e53: Pull complete
11048ebae908: Pull complete
c58094023a2e: Pull complete
1e8f13102fa0: Pull complete
8c1425d731a6: Pull complete
14e6f69e6aab: Pull complete
c90c2f3858cf: Pull complete
b78202ba9229: Pull complete
cadce28d1b9c: Pull complete
6fb2c5af5492: Pull complete
7a59522b36b8: Pull complete
722b05c4c4b1: Pull complete
bd4039b5406f: Pull complete
Digest: sha256:7b371982ac83beee40bee645c6efab1bc9113abbce9cbc95bf3eddac268adf57
Status: Downloaded newer image for mariadb:latest
be1ba3ee3e26bf14ab45f18bfb4c4ff2ceb8c24d16ada50f8cd7c5f71e1417c0
~~~

## 3.3. Mediawiki
`docker run --network xarxa_dades --name mi_wiki -e MYSQL_DATABASE=wiki -e MYSQL_USER=root -e MYSQL_PASSWORD=p@ssw0rd -e MYSQL_RANDOM_ROOT_PASSWORD='no' -p 8080:80 -d mediawiki`

`docker ps -a`

~~~

CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
32f712693175        wordpress           "docker-entrypoint.s…"   13 days ago         Exited (0) 6 minutes ago                        angry_wu
1ccd871b43d0        mediawiki           "docker-php-entrypoi…"   13 days ago         Exited (0) 54 seconds ago                       mi_wiki
aff2512e04f0        wordpress           "docker-entrypoint.s…"   2 weeks ago         Exited (0) 5 minutes ago                        mi_wordpress
be1ba3ee3e26        mariadb             "docker-entrypoint.s…"   2 weeks ago         Exited (0) 5 seconds ago                        mi_db
~~~
_**Aqui tenim els contenidors creats però no engegats**_

`docker start mi_wiki`

`docker start mi_db`

`docker ps -a`

~~~

CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                     PORTS                  NAMES
32f712693175        wordpress           "docker-entrypoint.s…"   13 days ago         Exited (0) 9 minutes ago                          angry_wu
1ccd871b43d0        mediawiki           "docker-php-entrypoi…"   13 days ago         Up 17 seconds              0.0.0.0:8080->80/tcp   mi_wiki
aff2512e04f0        wordpress           "docker-entrypoint.s…"   2 weeks ago         Exited (0) 8 minutes ago                          mi_wordpress
be1ba3ee3e26        mariadb             "docker-entrypoint.s…"   2 weeks ago         Up 11 seconds              3306/tcp               mi_db
~~~
_**Ara si que els tenim en marxa els contenidors.**_

_**Ja podem començar a configurar la nostra MediaWiki**_

![MediaWiki](mediawiki.png)

![MediaWiki](mediawiki_1.png)

![MediaWiki](mediawiki_2.png)

![MediaWiki](mediawiki_3.png)

![MediaWiki](mediawiki_4.png)

![MediaWiki](mediawiki_5.png)

![MediaWiki](mediawiki_6.png)

![MediaWiki](mediawiki_7.png)

![MediaWiki](mediawiki_8.png)

![MediaWiki](mediawiki_9.png)

`docker cp LocalSettings.php mi_wiki:/var/www/html`

`docker container ps`

~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
1ccd871b43d0        mediawiki           "docker-php-entrypoi…"   2 weeks ago         Up 42 minutes       0.0.0.0:8080->80/tcp   mi_wiki
be1ba3ee3e26        mariadb             "docker-entrypoint.s…"   2 weeks ago         Up 42 minutes       3306/tcp               mi_db
~~~

## 3.3. Salvar les dades
`docker commit -a "asmon10<alex_sernamontore@iescarlesvallbona.cat>" mi_wiki asmon10/m11:mi_wiki1`

`docker push asmon10/m11:mi_wiki1`

~~~
The push refers to repository [docker.io/asmon10/m11]
3db40a9b8fc6: Pushed 
bdc6d6667a88: Mounted from library/mediawiki 
a2bf92fd5640: Mounted from library/mediawiki 
d3669d99e940: Mounted from library/mediawiki 
00d3d1ff12b3: Mounted from library/mediawiki 
d1aa1aee5715: Mounted from library/mediawiki 
080d279394e3: Mounted from library/mediawiki 
6b86d24cb3ca: Mounted from library/mediawiki 
264d27be709e: Mounted from library/mediawiki 
8ff95ba52b1d: Mounted from library/mediawiki 
3023de69050d: Mounted from library/mediawiki 
6d8035894c24: Mounted from library/mediawiki 
2cf1609e6759: Mounted from library/mediawiki 
a8acd64544c1: Mounted from library/mediawiki 
ba31a1dbfcfb: Mounted from library/mediawiki 
3e84f33ac944: Mounted from library/mediawiki 
9f9d470ac131: Mounted from library/mediawiki 
86569e4ec54b: Mounted from library/mediawiki 
12fe3564ccac: Mounted from library/mediawiki 
4e9b2aba858c: Mounted from library/mediawiki 
b67d19e65ef6: Mounted from library/mediawiki
mi_wiki1: digest: sha256:5a39cd4c2a0261c0ccf165086aaca79f99e9531a604384a42787d9c3f5e6ce16 size: 4706
~~~
-----------
`docker commit -a "asmon10<alex_sernamontore@iescarlesvallbona.cat>" mi_db asmon10/m11:mi_db1`

`docker push asmon10/m11:mi_db1`

~~~
The push refers to repository [docker.io/asmon10/m11]
4838954bac4d: Pushed 
eb8ca3771ecb: Mounted from library/mariadb 
81d3996c1aed: Mounted from library/mariadb 
089a858e6791: Mounted from library/mariadb 
50389c2d9a93: Mounted from library/mariadb 
9f849aeb0482: Mounted from library/mariadb 
aa8761633e26: Mounted from library/mariadb 
bb1cad2a39f6: Mounted from library/mariadb 
19bc73cb9774: Mounted from library/mariadb 
257c972fd365: Mounted from library/mariadb 
379984748af8: Mounted from library/mariadb 
19331eff40f0: Mounted from library/mariadb 
100ef12ce3a4: Mounted from library/mariadb 
97e6b67a30f1: Mounted from library/mariadb 
a090697502b8: Mounted from library/mariadb 
mi_db1: digest: sha256:88a3bc2c5ce6a8235648786b4ae5642222d5ce7f3838c1c486388a241a0940d4 size: 3447
~~~

## 3.4. Esborrar-ho tot
`docker ps -a`

~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                           PORTS               NAMES
32f712693175        wordpress           "docker-entrypoint.s…"   2 weeks ago         Exited (0) 6 days ago                                angry_wu
1ccd871b43d0        mediawiki           "docker-php-entrypoi…"   2 weeks ago         Exited (137) 22 hours ago                            mi_wiki
aff2512e04f0        wordpress           "docker-entrypoint.s…"   2 weeks ago         Exited (0) 6 days ago                                mi_wordpress
be1ba3ee3e26        mariadb             "docker-entrypoint.s…"   2 weeks ago         Exited (255) About an hour ago   3306/tcp            mi_db
~~~

`docker start mi_db`

~~~
mi_db
~~~

`docker start mi_wiki`

~~~
mi_wiki
~~~

`docker kill mi_wiki `

~~~
mi_wiki
~~~

`docker kill mi_db`

~~~
mi_db
~~~

`docker rm mi_wiki `

~~~
mi_wiki
~~~

`docker rm mi_db`

~~~
mi_db
~~~

`docker ps -a`

~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                  PORTS               NAMES
32f712693175        wordpress           "docker-entrypoint.s…"   2 weeks ago         Exited (0) 6 days ago                       angry_wu
aff2512e04f0        wordpress           "docker-entrypoint.s…"   2 weeks ago         Exited (0) 6 days ago                       mi_wordpress
~~~

## 3.5. Recuperació de tot
`docker run --network xarxa_dades --name mi_wiki -p8080:80 -d asmon10/m11:mi_wiki1`

~~~
bf8aaf140cb4b30745a6a43c07471f110f26777292bc8190248e649bfd426c3f
~~~
`docker run --network xarxa_dades --name mi_db -d asmon10/m11:mi_db1`

~~~
121f0915a1572403d3b739e4f6317fced0e271e49d043407b224141afd7f38ef
~~~

`docker ps -a`

~~~
CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS                  PORTS                  NAMES
121f0915a157        asmon10/m11:mi_db1     "docker-entrypoint.s…"   3 minutes ago       Up 3 minutes            3306/tcp               mi_db2
bf8aaf140cb4        asmon10/m11:mi_wiki1   "docker-php-entrypoi…"   3 minutes ago       Up 3 minutes            0.0.0.0:8080->80/tcp   mi_wiki2
32f712693175        wordpress              "docker-entrypoint.s…"   2 weeks ago         Exited (0) 6 days ago                          angry_wu
aff2512e04f0        wordpress              "docker-entrypoint.s…"   2 weeks ago         Exited (0) 6 days ago                          mi_wordpress
~~~

_**Al recuperar les dades, la wiki dona le següent error:**_

![WikiError](mediawiki_error_1.png)

_**Arribat a aquest punt, no he sigut capaç d'avançar més**_

## 3.6. Joomla
`docker run --network xarxa_dades --name mi_jm -e JOOMLA_DB_HOST=mi_db -e JOOMLA_DB_USER=root -e JOOMLA_DB_PASSWORD=p@ssw0rd -e JOOMLA_DB_NAME=joomla -p81:80 -d joomla`

~~~
Unable to find image 'joomla:latest' locally
latest: Pulling from library/joomla
8d691f585fa8: Already exists 
cba12d3fd8b1: Already exists 
cda54d6474c8: Already exists 
412447ed0729: Already exists 
84de6fc539c3: Already exists 
d67567ed6145: Already exists 
22ca6c438da4: Already exists 
7b51c48b6603: Already exists 
ad31fb12f3e6: Already exists 
5aec5a38e823: Already exists 
111ec12030c9: Already exists 
66c21038613b: Already exists 
d523f7f23c9c: Already exists 
10cd43b07d51: Already exists 
7a2fee49a63f: Pull complete 
343816c97de1: Pull complete 
34181216d690: Pull complete 
cf6f1d4837d7: Pull complete 
dfeee1060b38: Pull complete 
Digest: sha256:60340c19aaf586daff068f746092eec847d71b31fa51ae8e3f02a06512d25780
Status: Downloaded newer image for joomla:latest
672eea798cb467530570d5cb2006ea4f5097b118548116b96fc9fe5632b7b68c
~~~

![Joomla](joomla_1.png)

![Joomla](joomla_2.png)

![Joomla](joomla_3.png)

![Joomla](joomla_4.png)

![Joomla](joomla_5.png)

![Joomla](joomla_6.png)

`docker commit -a "asmon10<alex_sernamontore@iescarlesvallbona.cat>" mi_jm asmon10/m11:mi_jm1`

~~~
sha256:c62a6e6415a91bdc25f92cb71b0a3f9a4929e94c96768d2748cdc4423be1d58e
~~~

`docker push asmon10/m11:mi_jm1`

~~~
The push refers to repository [docker.io/asmon10/m11]
842e84403706: Pushed 
4bfbe26e1a9a: Mounted from library/joomla 
f10cb85d701c: Mounted from library/joomla 
af10f9ab9109: Mounted from library/joomla 
fc2442c9b634: Mounted from library/joomla 
d8c144b49785: Mounted from library/joomla 
6b86d24cb3ca: Layer already exists 
264d27be709e: Layer already exists 
8ff95ba52b1d: Layer already exists 
3023de69050d: Layer already exists 
6d8035894c24: Layer already exists 
2cf1609e6759: Layer already exists 
a8acd64544c1: Layer already exists 
ba31a1dbfcfb: Layer already exists 
3e84f33ac944: Layer already exists 
9f9d470ac131: Layer already exists 
86569e4ec54b: Layer already exists 
12fe3564ccac: Layer already exists 
4e9b2aba858c: Layer already exists 
b67d19e65ef6: Layer already exists 
mi_jm1: digest: sha256:017927c3b8a24d3ac0c4f5fe3a29d8c14783ed7c198a75f25bb7aa90707bab5e size: 4494
~~~

`docker push asmon10/m11:mi_db1`

~~~
The push refers to repository [docker.io/asmon10/m11]
4838954bac4d: Layer already exists 
eb8ca3771ecb: Layer already exists 
81d3996c1aed: Layer already exists 
089a858e6791: Layer already exists 
50389c2d9a93: Layer already exists 
9f849aeb0482: Layer already exists 
aa8761633e26: Layer already exists 
bb1cad2a39f6: Layer already exists 
19bc73cb9774: Layer already exists 
257c972fd365: Layer already exists 
379984748af8: Layer already exists 
19331eff40f0: Layer already exists 
100ef12ce3a4: Layer already exists 
97e6b67a30f1: Layer already exists 
a090697502b8: Layer already exists 
mi_db1: digest: sha256:88a3bc2c5ce6a8235648786b4ae5642222d5ce7f3838c1c486388a241a0940d4 size: 3447
~~~
